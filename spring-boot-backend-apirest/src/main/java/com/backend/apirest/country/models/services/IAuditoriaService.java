package com.backend.apirest.country.models.services;

import java.util.List;

import com.backend.apirest.country.models.entity.Auditoria;

public interface IAuditoriaService {
	
	public List<Auditoria> findAll();
	
	public Auditoria save(Auditoria auditoria);
	
}
