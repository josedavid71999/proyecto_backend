package com.backend.apirest.country.models.entity;

import java.util.Map;

public class NombrePais {
	private String common;
    private String official;
    private Map<String, Map<String, String>> nativeName;
	public String getCommon() {
		return common;
	}
	public void setCommon(String common) {
		this.common = common;
	}
	public String getOfficial() {
		return official;
	}
	public void setOfficial(String official) {
		this.official = official;
	}
	public Map<String, Map<String, String>> getNativeName() {
		return nativeName;
	}
	public void setNativeName(Map<String, Map<String, String>> nativeName) {
		this.nativeName = nativeName;
	}
    
    
}
