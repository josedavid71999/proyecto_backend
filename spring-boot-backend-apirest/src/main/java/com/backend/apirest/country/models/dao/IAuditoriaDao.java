package com.backend.apirest.country.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.backend.apirest.country.models.entity.Auditoria;

public interface IAuditoriaDao extends CrudRepository<Auditoria, Long>{
	
}
