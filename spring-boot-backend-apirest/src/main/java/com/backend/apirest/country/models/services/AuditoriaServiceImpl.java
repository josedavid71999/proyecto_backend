package com.backend.apirest.country.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.backend.apirest.country.models.dao.IAuditoriaDao;
import com.backend.apirest.country.models.entity.Auditoria;

@Service
public class AuditoriaServiceImpl implements IAuditoriaService{
	
	@Autowired
	private IAuditoriaDao auditoriaDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<Auditoria> findAll() {
		
		return (List<Auditoria>) auditoriaDao.findAll();
		
	}

	@Override
	@Transactional
	public Auditoria save(Auditoria auditoria) {
		
		return auditoriaDao.save(auditoria);
	}

}
