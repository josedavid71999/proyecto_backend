package com.backend.apirest.country.models.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="Auditoria")
public class Auditoria {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long Id;
	
	@Column(name="AU_CREATION_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtCreation;
	
	@Column(name="AU_COUNTRY_NAME")
	private String strCountryName;
	
	//@Column(name="DATA_RESPONSE")
	@Column(columnDefinition = "LONGTEXT")
	private String strResponse;
	
	public Auditoria() {
		//
	}

	public Long getBiIdAuditoria() {
		return Id;
	}

	public void setBiIdAuditoria(Long Id) {
		this.Id = Id;
	}

	public Date getDtCreation() {
		return dtCreation;
	}

	public void setDtCreation(Date dtCreation) {
		this.dtCreation = dtCreation;
	}

	public String getStrCountryName() {
		return strCountryName;
	}

	public void setStrCountryName(String strCountryName) {
		this.strCountryName = strCountryName;
	}

	public String getStrResponse() {
		return strResponse;
	}

	public void setStrResponse(String strResponse) {
		this.strResponse = strResponse;
	}

	
}
