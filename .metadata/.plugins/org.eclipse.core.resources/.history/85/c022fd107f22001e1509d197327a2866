package com.backend.apirest.country.models.entity;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;


public class Pais {
	
	private NombrePais name;
    private List<String> tld;
    private String cca2;
    private String ccn3;
    private String cca3;
    private String cioc;
    private boolean independent;
    private String status;
    private boolean unMember;
    private Map<String, Moneda> currencies;
    private Map<String, List<String>> idd;
    private List<String> capital;
    private List<String> altSpellings;
    private String region;
    private String subregion;
    private Map<String, String> languages;
    private Map<String, Traduccion> translations;
    private List<Double> latlng;
    private boolean landlocked;
    private List<String> borders;
    private double area;
    private Map<String, Map<String, String>> demonyms;
    private String flag;
    private Map<String, String> maps;
    private long population;
    private Map<String, Double> gini;
    private String fifa;
    private Car car;
    private List<String> timezones;
    private List<String> continents;
    private Map<String, String> flags;
    private Map<String, String> coatOfArms;
    private String startOfWeek;
    private Map<String, List<Double>> capitalInfo;
    private Map<String, String> postalCode;
	public NombrePais getName() {
		return name;
	}
	public void setName(NombrePais name) {
		this.name = name;
	}
	public List<String> getTld() {
		return tld;
	}
	public void setTld(List<String> tld) {
		this.tld = tld;
	}
	public String getCca2() {
		return cca2;
	}
	public void setCca2(String cca2) {
		this.cca2 = cca2;
	}
	public String getCcn3() {
		return ccn3;
	}
	public void setCcn3(String ccn3) {
		this.ccn3 = ccn3;
	}
	public String getCca3() {
		return cca3;
	}
	public void setCca3(String cca3) {
		this.cca3 = cca3;
	}
	public String getCioc() {
		return cioc;
	}
	public void setCioc(String cioc) {
		this.cioc = cioc;
	}
	public boolean isIndependent() {
		return independent;
	}
	public void setIndependent(boolean independent) {
		this.independent = independent;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public boolean isUnMember() {
		return unMember;
	}
	public void setUnMember(boolean unMember) {
		this.unMember = unMember;
	}
	public Map<String, Moneda> getCurrencies() {
		return currencies;
	}
	public void setCurrencies(Map<String, Moneda> currencies) {
		this.currencies = currencies;
	}
	public Map<String, List<String>> getIdd() {
		return idd;
	}
	public void setIdd(Map<String, List<String>> idd) {
		this.idd = idd;
	}
	public List<String> getCapital() {
		return capital;
	}
	public void setCapital(List<String> capital) {
		this.capital = capital;
	}
	public List<String> getAltSpellings() {
		return altSpellings;
	}
	public void setAltSpellings(List<String> altSpellings) {
		this.altSpellings = altSpellings;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getSubregion() {
		return subregion;
	}
	public void setSubregion(String subregion) {
		this.subregion = subregion;
	}
	public Map<String, String> getLanguages() {
		return languages;
	}
	public void setLanguages(Map<String, String> languages) {
		this.languages = languages;
	}
	public Map<String, Traduccion> getTranslations() {
		return translations;
	}
	public void setTranslations(Map<String, Traduccion> translations) {
		this.translations = translations;
	}
	public List<Double> getLatlng() {
		return latlng;
	}
	public void setLatlng(List<Double> latlng) {
		this.latlng = latlng;
	}
	public boolean isLandlocked() {
		return landlocked;
	}
	public void setLandlocked(boolean landlocked) {
		this.landlocked = landlocked;
	}
	public List<String> getBorders() {
		return borders;
	}
	public void setBorders(List<String> borders) {
		this.borders = borders;
	}
	public double getArea() {
		return area;
	}
	public void setArea(double area) {
		this.area = area;
	}
	public Map<String, Map<String, String>> getDemonyms() {
		return demonyms;
	}
	public void setDemonyms(Map<String, Map<String, String>> demonyms) {
		this.demonyms = demonyms;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public Map<String, String> getMaps() {
		return maps;
	}
	public void setMaps(Map<String, String> maps) {
		this.maps = maps;
	}
	public long getPopulation() {
		return population;
	}
	public void setPopulation(long population) {
		this.population = population;
	}
	public Map<String, Double> getGini() {
		return gini;
	}
	public void setGini(Map<String, Double> gini) {
		this.gini = gini;
	}
	public String getFifa() {
		return fifa;
	}
	public void setFifa(String fifa) {
		this.fifa = fifa;
	}
	public Car getCar() {
		return car;
	}
	public void setCar(Car car) {
		this.car = car;
	}
	public List<String> getTimezones() {
		return timezones;
	}
	public void setTimezones(List<String> timezones) {
		this.timezones = timezones;
	}
	public List<String> getContinents() {
		return continents;
	}
	public void setContinents(List<String> continents) {
		this.continents = continents;
	}
	public Map<String, String> getFlags() {
		return flags;
	}
	public void setFlags(Map<String, String> flags) {
		this.flags = flags;
	}
	public Map<String, String> getCoatOfArms() {
		return coatOfArms;
	}
	public void setCoatOfArms(Map<String, String> coatOfArms) {
		this.coatOfArms = coatOfArms;
	}
	public String getStartOfWeek() {
		return startOfWeek;
	}
	public void setStartOfWeek(String startOfWeek) {
		this.startOfWeek = startOfWeek;
	}
	public Map<String, List<Double>> getCapitalInfo() {
		return capitalInfo;
	}
	public void setCapitalInfo(Map<String, List<Double>> capitalInfo) {
		this.capitalInfo = capitalInfo;
	}
	public Map<String, String> getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(Map<String, String> postalCode) {
		this.postalCode = postalCode;
	}
	
	
}
