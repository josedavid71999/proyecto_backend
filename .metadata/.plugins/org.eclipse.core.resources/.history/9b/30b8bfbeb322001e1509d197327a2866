package com.backend.apirest.country.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.backend.apirest.country.models.entity.Auditoria;
import com.backend.apirest.country.models.entity.Moneda;
import com.backend.apirest.country.models.entity.Pais;
import com.backend.apirest.country.models.services.IAuditoriaService;
import com.backend.apirest.country.responseBean.PaisesResponseBean;

import org.springframework.web.client.RestTemplate;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/api")
@Slf4j
public class PaisRestController {
	
	private final RestTemplate restTemplate;
	
	public PaisRestController(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}
	
	@Autowired
	private IAuditoriaService auditoriaService;
	
	@GetMapping("/auditoria")
	public List<Auditoria> index(){
		
		return auditoriaService.findAll();
		//return null;
	}
	
	@GetMapping("/pais")
	public List<PaisesResponseBean> getPaises() throws Exception{
		List<PaisesResponseBean> lstResponse = new ArrayList<>();
		try {
			RestTemplate restTemplate = new RestTemplate();
			ObjectMapper objectMapper = new ObjectMapper();
			
			String apiUrl = "https://restcountries.com/v3.1/name/Br";
			String jsonResponse = restTemplate.getForObject(apiUrl, String.class);
			
			Pais[] paises = objectMapper.readValue(jsonResponse, Pais[].class);
			
			
			for(Pais objPais : paises) {
				
				PaisesResponseBean objResponse = new PaisesResponseBean();
				objResponse.setNombre(objPais.getName().getOfficial());
				objResponse.setRegion(objPais.getRegion());
				objResponse.setSubregion(objPais.getSubregion());
				objResponse.setLanguages(getLanguages(objPais.getLanguages()));
				objResponse.setCurrencies(getCurrencies(objPais.getCurrencies()));
				objResponse.setCoatOfArms(objPais.getCoatOfArms().get("svg"));
				lstResponse.add(objResponse);
			}
			
		} catch (Exception e) {
			
		}
		
		return lstResponse;
		
	}
	
	private String getCurrencies(Map<String, Moneda> currencies){
		StringBuilder strCurrencies = new StringBuilder();
		currencies.forEach((key,value) -> {
			if(strCurrencies.isEmpty()) {
				strCurrencies.append(value.getName());
			}else {
				strCurrencies.append(" - ").append(value.getName());
			}
		});
		return strCurrencies.toString();
	}
	
	private String getLanguages(Map<String, String> languages) {
		StringBuilder strLanguages = new StringBuilder();
		languages.forEach((key,value) -> {
			if(strLanguages.isEmpty()) {
				strLanguages.append(value);
			}else {
				strLanguages.append(" - ").append(value);
			}
		});
		return strLanguages.toString();
	}
	
	@GetMapping("/paises/{name}")
	public List<PaisesResponseBean> getCountrys(@PathVariable String name) throws Exception{
		List<PaisesResponseBean> lstResponse = new ArrayList<>();
		try {
			RestTemplate restTemplate = new RestTemplate();
			ObjectMapper objectMapper = new ObjectMapper();
			
			String apiUrl = "https://restcountries.com/v3.1/name/" + name;
			String jsonResponse = restTemplate.getForObject(apiUrl, String.class);
			
			Pais[] paises = objectMapper.readValue(jsonResponse, Pais[].class);
			
			
			for(Pais objPais : paises) {
				PaisesResponseBean objResponse = new PaisesResponseBean();
				objResponse.setNombre(objPais.getName().getOfficial());
				objResponse.setRegion(objPais.getRegion());
				objResponse.setSubregion(objPais.getSubregion());
				objResponse.setLanguages(getLanguages(objPais.getLanguages()));
				objResponse.setCurrencies(getCurrencies(objPais.getCurrencies()));
				objResponse.setCoatOfArms(objPais.getCoatOfArms().get("svg"));
				lstResponse.add(objResponse);
			}
			
		} catch (Exception e) {
			
		}
		
		return lstResponse;
		
	}
	
	
	
	
	
	@GetMapping("/consumir")
	public ResponseEntity<Pais[]> getPais() {
		
		String uri = "https://restcountries.com/v3.1/name/Brasil";
		ResponseEntity<Pais[]> response = restTemplate.getForEntity(uri, Pais[].class);
		response.getStatusCode();
		return response;
	}
	
}
